var datanhathuoc = [{
        "STT": "1  ",
        "ten_co_so": "Nhà Thuốc Thanh Nữ   ",
        "dia_chi": "Lô số 20 chợ Đông Ba, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4726645  ",
        "Latitude": "107.5886657 "
    },
    {
        "STT": "2  ",
        "ten_co_so": "Nhà Thuốc Tân Quang  ",
        "dia_chi": "Lô số 19 chợ Đông Ba, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4726645  ",
        "Latitude": "107.5886657 "
    },
    {
        "STT": "3  ",
        "ten_co_so": "Nhà Thuốc Nhật Lan   ",
        "dia_chi": "Lô B chợ Đông Ba, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.472386   ",
        "Latitude": "107.588983  "
    },
    {
        "STT": "4  ",
        "ten_co_so": "Nhà Thuốc Minh Khoa  ",
        "dia_chi": "Chợ Đông Ba, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4726645  ",
        "Latitude": "107.5886657 "
    },
    {
        "STT": "5  ",
        "ten_co_so": "Công Ty LC Pharma ",
        "dia_chi": "17 Hai Bà Trưng, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.460774   ",
        "Latitude": "107.588936  "
    },
    {
        "STT": "6  ",
        "ten_co_so": "Nhà Thuốc Phong Hà 2 ",
        "dia_chi": "1 Hai Bà Trưng, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4623959  ",
        "Latitude": "107.58967   "
    },
    {
        "STT": "7  ",
        "ten_co_so": "Nhà Thuốc Thành Phước   ",
        "dia_chi": "2 Hai Bà Trưng, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4619771  ",
        "Latitude": "107.5889303 "
    },
    {
        "STT": "8  ",
        "ten_co_so": "Công Ty Thuận Thảo   ",
        "dia_chi": "34 Ngô Quyền, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4628056  ",
        "Latitude": "107.5901681 "
    },
    {
        "STT": "9  ",
        "ten_co_so": "Nhà Thuốc Thanh Châu ",
        "dia_chi": "30 Ngô Quyền, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4611122  ",
        "Latitude": "107.5879105 "
    },
    {
        "STT": "10 ",
        "ten_co_so": "Nhà Thuốc Thanh Hải  ",
        "dia_chi": "36 Ngô Quyền, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.460874   ",
        "Latitude": "107.588585  "
    },
    {
        "STT": "11 ",
        "ten_co_so": "Nhà Thuốc Thuận Thái ",
        "dia_chi": "16 Ngô Quyền, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.460999   ",
        "Latitude": "107.58793   "
    },
    {
        "STT": "12 ",
        "ten_co_so": "Nhà Thuốc Phương Nhi ",
        "dia_chi": "16 Ngô Quyền, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.460999   ",
        "Latitude": "107.58793   "
    },
    {
        "STT": "13 ",
        "ten_co_so": "Nhà Thuốc Thu Hương  ",
        "dia_chi": "36 Đặng Huy Trứ, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4484603  ",
        "Latitude": "107.5863597 "
    },
    {
        "STT": "14 ",
        "ten_co_so": "Công Ty Hồng Lan  ",
        "dia_chi": "3 Lê Thánh Tôn, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.473347   ",
        "Latitude": "107.583605  "
    },
    {
        "STT": "15 ",
        "ten_co_so": "Nhà Thuốc Bình An-Trần Thị Phú Cần  ",
        "dia_chi": "6 Phạm văn đồng, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.472304   ",
        "Latitude": "107.6005161 "
    },
    {
        "STT": "16 ",
        "ten_co_so": "Nhà Thuốc Phú Thượng ",
        "dia_chi": "Phạm Văn Đồng, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4815352  ",
        "Latitude": "107.6014844 "
    },
    {
        "STT": "17 ",
        "ten_co_so": "Nhà Thuốc Vĩnh Nghĩa ",
        "dia_chi": "07 Phạm Văn đồng, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4726727  ",
        "Latitude": "107.6005122 "
    },
    {
        "STT": "18 ",
        "ten_co_so": "Nhà Thuốc kim Anh ",
        "dia_chi": "179 Nguyễn Sinh Cung, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4825716  ",
        "Latitude": "107.5960321 "
    },
    {
        "STT": "19 ",
        "ten_co_so": "Nhà Thuốc Bến Ngự ",
        "dia_chi": "41 Phan Bội Châu, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4556243  ",
        "Latitude": "107.5838772 "
    },
    {
        "STT": "20 ",
        "ten_co_so": "Nhà Thuốc Vĩnh Phú   ",
        "dia_chi": "20 Phan Bội Châu, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.457003   ",
        "Latitude": "107.58405   "
    },
    {
        "STT": "21 ",
        "ten_co_so": "Hiệu Thuốc số 8   ",
        "dia_chi": "39 Phan Bội Châu, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.455654   ",
        "Latitude": "107.584044  "
    },
    {
        "STT": "22 ",
        "ten_co_so": "Nhà Thuốc Nhật Thịnh-Đỗ Thị thiên hương   ",
        "dia_chi": "Hồ Đắc Di, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4427263  ",
        "Latitude": "107.6046642 "
    },
    {
        "STT": "23 ",
        "ten_co_so": "Nhà Thuốc Ngô Quý Thích ",
        "dia_chi": "157 Trần Hưng Đạo, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4712492  ",
        "Latitude": "107.5870457 "
    },
    {
        "STT": "24 ",
        "ten_co_so": "Nhà Thuốc Bích Ngọc  ",
        "dia_chi": "143 Đinh Tiên Hoàng, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4813063  ",
        "Latitude": "107.5752067 "
    },
    {
        "STT": "25 ",
        "ten_co_so": "Nhà Thuốc Thượng Tứ  ",
        "dia_chi": "15 Đinh Tiên Hoàng, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.469204   ",
        "Latitude": "107.584613  "
    },
    {
        "STT": "26 ",
        "ten_co_so": "Nhà Thuốc Bảo Hưng   ",
        "dia_chi": "34 Bà Triệu, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.468415   ",
        "Latitude": "107.5987887 "
    },
    {
        "STT": "27 ",
        "ten_co_so": "Nhà Thuốc Số 2 Phú Bài  ",
        "dia_chi": "Khu 4 TT Phú Bài, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3869801  ",
        "Latitude": "107.6984879 "
    },
    {
        "STT": "28 ",
        "ten_co_so": "Hiệu số 1   ",
        "dia_chi": "Chợ Trường An, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4433398  ",
        "Latitude": "107.5849378 "
    },
    {
        "STT": "29 ",
        "ten_co_so": "Quầy Thuốc Ca May ",
        "dia_chi": "Huyện A Lưới, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.2303741  ",
        "Latitude": "107.3375791 "
    },
    {
        "STT": "30 ",
        "ten_co_so": "Quầy Thuốc Út Thương ",
        "dia_chi": "Huyện A Lưới, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.2303741  ",
        "Latitude": "107.3375791 "
    },
    {
        "STT": "31 ",
        "ten_co_so": "Nhà Thuốc Hùng Vương ",
        "dia_chi": "99 Mai Thúc Loan, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4761297  ",
        "Latitude": "107.5818848 "
    },
    {
        "STT": "32 ",
        "ten_co_so": "Nhà Thuốc Thanh Toàn ",
        "dia_chi": "250 Hùng Vương, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4542325  ",
        "Latitude": "107.6004558 "
    },
    {
        "STT": "33 ",
        "ten_co_so": "Nhà Thuốc Hương Bình ",
        "dia_chi": "119 Hùng Vương, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.458761   ",
        "Latitude": "107.599936  "
    },
    {
        "STT": "34 ",
        "ten_co_so": "Nhà Thuốc Quang Đạo  ",
        "dia_chi": "76 Chi Lăng, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4768876  ",
        "Latitude": "107.5896214 "
    },
    {
        "STT": "35 ",
        "ten_co_so": "Nhà Thuốc Ngọc Diệp  ",
        "dia_chi": "192B Nguyễn Trãi, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4745381  ",
        "Latitude": "107.5672693 "
    },
    {
        "STT": "36 ",
        "ten_co_so": "Nhà Thuốc Phương Vinh 2 ",
        "dia_chi": "197 Nguyễn Trãi, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.472623   ",
        "Latitude": "107.568751  "
    },
    {
        "STT": "37 ",
        "ten_co_so": "Nhà Thuốc Chính Đức  ",
        "dia_chi": "289 bùi thị xuân, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.452437   ",
        "Latitude": "107.560437  "
    },
    {
        "STT": "38 ",
        "ten_co_so": "Nhà Thuốc Trung Hậu  ",
        "dia_chi": "1 Trường Chinh, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4622902  ",
        "Latitude": "107.600239  "
    },
    {
        "STT": "39 ",
        "ten_co_so": "Nhà Thuốc Nam Giao   ",
        "dia_chi": "336 Điện Biên Phủ, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4390615  ",
        "Latitude": "107.5820146 "
    },
    {
        "STT": "40 ",
        "ten_co_so": "Nhà Thuốc Bảo Hoàng  ",
        "dia_chi": "Khu 3 TT Phú Bài, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3869801  ",
        "Latitude": "107.6984879 "
    },
    {
        "STT": "41 ",
        "ten_co_so": "Nhà Thuốc Phú Đức-Nguyễn phú đức ",
        "dia_chi": "10 Ngự bình, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4528103  ",
        "Latitude": "107.600191  "
    },
    {
        "STT": "42 ",
        "ten_co_so": "Nhà Thuốc TâyLộc  ",
        "dia_chi": "58 Trần Quốc Toản, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.473704   ",
        "Latitude": "107.565951  "
    },
    {
        "STT": "43 ",
        "ten_co_so": "Nhà Thuốc Thuỷ Tiên  ",
        "dia_chi": "107 Hoàng Diệu, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4731131  ",
        "Latitude": "107.5682237 "
    },
    {
        "STT": "44 ",
        "ten_co_so": "Nhà Thuốc Thu Ngân   ",
        "dia_chi": "18 nguyễn phúc tần, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4700931  ",
        "Latitude": "107.5607687 "
    },
    {
        "STT": "45 ",
        "ten_co_so": "Nhà Thuốc Trịnh Thị Gái ",
        "dia_chi": "Chợ An Lỗ, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.503112   ",
        "Latitude": "107.3375791 "
    },
    {
        "STT": "46 ",
        "ten_co_so": "Quầy thuốc Hương Tú  ",
        "dia_chi": "Thôn 1, Vĩnh Thành, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4354994  ",
        "Latitude": "107.7932319 "
    },
    {
        "STT": "47 ",
        "ten_co_so": "Đại Lý Na Thức ",
        "dia_chi": "Thủy Dương, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4411867  ",
        "Latitude": "107.620996  "
    },
    {
        "STT": "48 ",
        "ten_co_so": "Nhà thuốc Tuấn Kiệt  ",
        "dia_chi": "Chợ Phù Bài, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3742303  ",
        "Latitude": "107.7213104 "
    },
    {
        "STT": "49 ",
        "ten_co_so": "Đại Lý Tứ Hạ   ",
        "dia_chi": "Tứ Hạ, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5322989  ",
        "Latitude": "107.4617707 "
    },
    {
        "STT": "50 ",
        "ten_co_so": "Đại Lý Thuốc Tây  ",
        "dia_chi": "105 Bao Vinh, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4967663  ",
        "Latitude": "107.5764039 "
    },
    {
        "STT": "51 ",
        "ten_co_so": "Nhà Thuốc phương lan ",
        "dia_chi": "ql 1A lộc bổng, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3414907  ",
        "Latitude": "107.7415037 "
    },
    {
        "STT": "52 ",
        "ten_co_so": "Quầy thuốc Nhật Tân  ",
        "dia_chi": "Lộc Bổn, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3282072  ",
        "Latitude": "107.7221709 "
    },
    {
        "STT": "53 ",
        "ten_co_so": "Nhà Thuốc Minh Hồng  ",
        "dia_chi": "Lộc Sơn, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3261637  ",
        "Latitude": "107.7458559 "
    },
    {
        "STT": "54 ",
        "ten_co_so": "Nhà Thuốc Mai Giỏi   ",
        "dia_chi": "Chợ Cầu 2, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.2713221  ",
        "Latitude": "107.8798031 "
    }
];