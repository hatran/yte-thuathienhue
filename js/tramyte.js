var datatramyte = [{
        "STT": "1  ",
        "Name": "Trạm y tế phường An Hoà ",
        "dia_chi": "Phường An Hòà, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4879574  ",
        "Latitude": "107.5506967 "
    },
    {
        "STT": "2  ",
        "Name": "Trạm y tế phường An Đông   ",
        "dia_chi": "Kiệt 2 Đặng Văn Ngữ, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4585787  ",
        "Latitude": "107.6019625 "
    },
    {
        "STT": "3  ",
        "Name": "Trạm y tế phường Thủy Biều ",
        "dia_chi": "Xã Thủy Biều, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.444564   ",
        "Latitude": "107.5511163 "
    },
    {
        "STT": "4  ",
        "Name": "Trạm y tế phường Thủy Xuân ",
        "dia_chi": "Xã Thủy Xuân, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4364925  ",
        "Latitude": "107.5696541 "
    },
    {
        "STT": "5  ",
        "Name": "Trạm y tế phường Hương Long   ",
        "dia_chi": "Xã Hương Long, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4633848  ",
        "Latitude": "107.5427 "
    },
    {
        "STT": "6  ",
        "Name": "Trạm y tế phường Thuận Lộc ",
        "dia_chi": "Phường Thuận Lộc, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4835656  ",
        "Latitude": "107.5740145 "
    },
    {
        "STT": "7  ",
        "Name": "Trạm y tế phường An Cựu ",
        "dia_chi": "Phường An Cựu, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4474711  ",
        "Latitude": "107.5961397 "
    },
    {
        "STT": "8  ",
        "Name": "Trạm y tế phường Phường Đúc   ",
        "dia_chi": "Phường Phường Đúc, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4564246  ",
        "Latitude": "107.5745895 "
    },
    {
        "STT": "9  ",
        "Name": "Trạm y tế phường Kim Long  ",
        "dia_chi": "Phường Kim Long, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4651608  ",
        "Latitude": "107.5605035 "
    },
    {
        "STT": "10 ",
        "Name": "Trạm y tế phường Phú Bình  ",
        "dia_chi": "Phường Phú Bình, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4889298  ",
        "Latitude": "107.5778841 "
    },
    {
        "STT": "11 ",
        "Name": "Trạm y tế phường Phú Cát   ",
        "dia_chi": "Phường Phú Cát, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4771961  ",
        "Latitude": "107.5904599 "
    },
    {
        "STT": "12 ",
        "Name": "Trạm y tế phường Phú Hậu   ",
        "dia_chi": "Phường Phú Hậu, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4919628  ",
        "Latitude": "107.5869252 "
    },
    {
        "STT": "13 ",
        "Name": "Trạm y tế phường Phú Hiệp  ",
        "dia_chi": "Phường Phú Hiệp, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4862258  ",
        "Latitude": "107.5845418 "
    },
    {
        "STT": "14 ",
        "Name": "Trạm y tế phường Phú Hòa   ",
        "dia_chi": "Phường Phú Hòa, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4709983  ",
        "Latitude": "107.5882406 "
    },
    {
        "STT": "15 ",
        "Name": "Trạm y tế phường Phú Hội   ",
        "dia_chi": "Phường Phú Hội, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4672825  ",
        "Latitude": "107.5992422 "
    },
    {
        "STT": "16 ",
        "Name": "Trạm y tế phường Phú Nhuận ",
        "dia_chi": "Phường Phú Nhuận, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.459649   ",
        "Latitude": "107.5963782 "
    },
    {
        "STT": "17 ",
        "Name": "Trạm y tế phường Phú Thuận ",
        "dia_chi": "Phường Phú Thuận, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4704555  ",
        "Latitude": "107.5630585 "
    },
    {
        "STT": "18 ",
        "Name": "Trạm y tế phường Phước Vĩnh   ",
        "dia_chi": "Phường Phước Vĩnh, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4512771  ",
        "Latitude": "107.5898933 "
    },
    {
        "STT": "19 ",
        "Name": "Trạm y tế phường Tây Lộc   ",
        "dia_chi": "Phường TÂY Lộc, Thành phố Huế, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4766867  ",
        "Latitude": "107.5655228 "
    },
    {
        "STT": "20 ",
        "Name": "Trạm y tế phường Thuận Hoà ",
        "dia_chi": "Phường Thuận Hòa, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4680421  ",
        "Latitude": "107.5722108 "
    },
    {
        "STT": "21 ",
        "Name": "Trạm y tế phường Thuận Thành  ",
        "dia_chi": "Phường Thuận Thành, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4717352  ",
        "Latitude": "107.5796877 "
    },
    {
        "STT": "22 ",
        "Name": "Trạm y tế phường Trường An ",
        "dia_chi": "Phường Trường An, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4442134  ",
        "Latitude": "107.5828018 "
    },
    {
        "STT": "23 ",
        "Name": "Trạm y tế phường Vĩnh Ninh ",
        "dia_chi": "Phường Vĩnh Ninh, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.459998   ",
        "Latitude": "107.5855014 "
    },
    {
        "STT": "24 ",
        "Name": "Trạm y tế phường Vỹ Dạ  ",
        "dia_chi": "Phường Vĩ Dạ, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4784301  ",
        "Latitude": "107.5986752 "
    },
    {
        "STT": "25 ",
        "Name": "Trạm y tế phường Xuân Phú  ",
        "dia_chi": "Phường Xuân Phú, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4676168  ",
        "Latitude": "107.6057621 "
    },
    {
        "STT": "26 ",
        "Name": "Trạm y tế xã Điền Môn   ",
        "dia_chi": "Xã Điền Môn, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.7080047  ",
        "Latitude": "107.4026244 "
    },
    {
        "STT": "27 ",
        "Name": "Trạm y tế xã Điền Hương ",
        "dia_chi": "Xã Điền Hương, Huyện Phong Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.7317891  ",
        "Latitude": "107.3907968 "
    },
    {
        "STT": "28 ",
        "Name": "Trạm y tế xã Điền Hoà   ",
        "dia_chi": "Xã Điền Hòa, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.6811603  ",
        "Latitude": "107.4499404 "
    },
    {
        "STT": "29 ",
        "Name": "Trạm y tế xã Điền Lộc   ",
        "dia_chi": "Xã Điền Lộc, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.704939   ",
        "Latitude": "107.4381106 "
    },
    {
        "STT": "30 ",
        "Name": "Trạm y tế xã Phong An   ",
        "dia_chi": "Xã Phong An, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5477246  ",
        "Latitude": "107.4144526 "
    },
    {
        "STT": "31 ",
        "Name": "Trạm y tế xã Phong Bình ",
        "dia_chi": "Xã Phong Bình, Huyện Phong Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.689317   ",
        "Latitude": "107.3553171 "
    },
    {
        "STT": "32 ",
        "Name": "Trạm y tế xã Phong Chương  ",
        "dia_chi": "Xã Phong Chương, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.6459289  ",
        "Latitude": "107.3967105 "
    },
    {
        "STT": "33 ",
        "Name": "Trạm y tế xã Phong Hải  ",
        "dia_chi": "Xã Phong Hải, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.6667147  ",
        "Latitude": "107.485433  "
    },
    {
        "STT": "34 ",
        "Name": "Trạm y tế xã Phong Hiền ",
        "dia_chi": "Xã Phong Hiền, Huyện Phong Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5756471  ",
        "Latitude": "107.4203669 "
    },
    {
        "STT": "35 ",
        "Name": "Trạm y tế xã Phong Hoà  ",
        "dia_chi": "Xã Phong Hòa, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.6272326  ",
        "Latitude": "107.3494043 "
    },
    {
        "STT": "36 ",
        "Name": "Trạm y tế xã Phong Mỹ   ",
        "dia_chi": "Xã Phong Mỹ, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4903752  ",
        "Latitude": "107.2193578 "
    },
    {
        "STT": "37 ",
        "Name": "Trạm y tế xã Phong Sơn  ",
        "dia_chi": "Xã Phong Sơn, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4918673  ",
        "Latitude": "107.4033243 "
    },
    {
        "STT": "38 ",
        "Name": "Trạm y tế xã Phong Xuân ",
        "dia_chi": "Xã Phong Xuân, Huyện Phong Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4285201  ",
        "Latitude": "107.2784615 "
    },
    {
        "STT": "39 ",
        "Name": "Trạm y tế xã Phong Thu  ",
        "dia_chi": "Xã Phong Thu, Huyện Phong Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5775494  ",
        "Latitude": "107.3316667 "
    },
    {
        "STT": "40 ",
        "Name": "Trạm y tế xã Điền Hải   ",
        "dia_chi": "Xã Điền Hải, Huyện Phong Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.6449894  ",
        "Latitude": "107.4736016 "
    },
    {
        "STT": "41 ",
        "Name": "Trạm y tế thị trấn Phong Điền ",
        "dia_chi": "Thị trấn Phong Điền, Huyện Phong Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5745189  ",
        "Latitude": "107.3671431 "
    },
    {
        "STT": "42 ",
        "Name": "Trạm y tế xã Quảng An   ",
        "dia_chi": "Xã Quảng An, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5670258  ",
        "Latitude": "107.5534739 "
    },
    {
        "STT": "43 ",
        "Name": "Trạm y tế xã Quảng Công ",
        "dia_chi": "Xã Quảng Công, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.6037112  ",
        "Latitude": "107.5564326 "
    },
    {
        "STT": "44 ",
        "Name": "Trạm y tế xã Quảng Lợi  ",
        "dia_chi": "Xã Quảng Lợi, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.617067   ",
        "Latitude": "107.4676861 "
    },
    {
        "STT": "45 ",
        "Name": "Trạm y tế xã Quảng Ngạn ",
        "dia_chi": "Xã Quảng Ngạn, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.6305481  ",
        "Latitude": "107.5090974 "
    },
    {
        "STT": "46 ",
        "Name": "Trạm y tế xã Quảng Phước   ",
        "dia_chi": "Xã Quảng Phước, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5856103  ",
        "Latitude": "107.5357224 "
    },
    {
        "STT": "47 ",
        "Name": "Trạm y tế xã Quảng Phú  ",
        "dia_chi": "Xã Quảng Phú, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5406164  ",
        "Latitude": "107.497265  "
    },
    {
        "STT": "48 ",
        "Name": "Trạm y tế xã Quảng Thái ",
        "dia_chi": "Xã Quảng Thái, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.637696   ",
        "Latitude": "107.4262813 "
    },
    {
        "STT": "49 ",
        "Name": "Trạm y tế xã Quảng Thành   ",
        "dia_chi": "Xã Quảng Thành, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5489556  ",
        "Latitude": "107.5653089 "
    },
    {
        "STT": "50 ",
        "Name": "Trạm y tế xã Quảng Thọ  ",
        "dia_chi": "Xã Quảng Thọ, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5411655  ",
        "Latitude": "107.5238888 "
    },
    {
        "STT": "51 ",
        "Name": "Trạm y tế xã Quảng Vinh ",
        "dia_chi": "Xã Quảng Vinh, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5643702  ",
        "Latitude": "107.485433  "
    },
    {
        "STT": "52 ",
        "Name": "Trạm y tế thị trấn Sịa  ",
        "dia_chi": "Thị trấn Sịa, Huyện Quảng Điền, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5850679  ",
        "Latitude": "107.5090974 "
    },
    {
        "STT": "53 ",
        "Name": "Trạm y tế Xã Hương An   ",
        "dia_chi": "Phường Hương An, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4658783  ",
        "Latitude": "107.5200488 "
    },
    {
        "STT": "54 ",
        "Name": "Trạm y tế Xã Hương Bình ",
        "dia_chi": "Xã Hương Bình, Huyện Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3897472  ",
        "Latitude": "107.4676861 "
    },
    {
        "STT": "55 ",
        "Name": "Trạm y tế Xã Hương Chữ  ",
        "dia_chi": "Phường Hương Chữ, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4931222  ",
        "Latitude": "107.5209304 "
    },
    {
        "STT": "56 ",
        "Name": "Trạm y tế Xã Hương Phong   ",
        "dia_chi": "Xã Hương Phong, Huyện Hương Trà, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5665378  ",
        "Latitude": "107.5919395 "
    },
    {
        "STT": "57 ",
        "Name": "Trạm y tế Xã Hương Thọ  ",
        "dia_chi": "Xã Hương Thọ, Huyện Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3816618  ",
        "Latitude": "107.5623501 "
    },
    {
        "STT": "58 ",
        "Name": "Trạm y tế Xã Hương Toàn ",
        "dia_chi": "Xã Hương Toàn, Huyện Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5128363  ",
        "Latitude": "107.5348121 "
    },
    {
        "STT": "59 ",
        "Name": "Trạm y tế Xã Hương Văn  ",
        "dia_chi": "Phường Hương Văn, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3957812  ",
        "Latitude": "107.3967105 "
    },
    {
        "STT": "60 ",
        "Name": "Trạm y tế Xã Hương Vân  ",
        "dia_chi": "Phường Hương Vân, Phường Hương Trà, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3957812  ",
        "Latitude": "107.3967105 "
    },
    {
        "STT": "61 ",
        "Name": "Trạm y tế Xã Hương Vinh ",
        "dia_chi": "Xã Hương Vinh, Huyện Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5160653  ",
        "Latitude": "107.571172  "
    },
    {
        "STT": "62 ",
        "Name": "Trạm y tế Xã Hương Xuân ",
        "dia_chi": "Phường Hương Xuân, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4822178  ",
        "Latitude": "107.4824751 "
    },
    {
        "STT": "63 ",
        "Name": "Trạm y tế Xã HảI Dương  ",
        "dia_chi": "Xã Hải Dương, Huyện Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5799729  ",
        "Latitude": "107.6008169 "
    },
    {
        "STT": "64 ",
        "Name": "Trạm y tế Xã Hương Hồ   ",
        "dia_chi": "Phường Hương Hồ, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.431143   ",
        "Latitude": "107.5150139 "
    },
    {
        "STT": "65 ",
        "Name": "Trạm y tế Xã Bình Điền  ",
        "dia_chi": "Xã Bình Điền, Huyện Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3008984  ",
        "Latitude": "107.4440254 "
    },
    {
        "STT": "66 ",
        "Name": "Trạm y tế Xã Bình Thành ",
        "dia_chi": "Xã Bình Thành, Huyện Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3175813  ",
        "Latitude": "107.5150139 "
    },
    {
        "STT": "67 ",
        "Name": "Trạm y tế Xã Hồng Tiến  ",
        "dia_chi": "Xã Hồng Tiến, Huyện Hương Trà, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3602712  ",
        "Latitude": "107.4469829 "
    },
    {
        "STT": "68 ",
        "Name": "Trạm y tế Thị trấn Tứ Hạ   ",
        "dia_chi": "Phường Tứ Hạ, Thị xã Hương Trà, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5322989  ",
        "Latitude": "107.4617707 "
    },
    {
        "STT": "69 ",
        "Name": "Trạm y tế Thị trấn Thuận An   ",
        "dia_chi": "Thị trấn Thuận An, Huyện Phú Vang, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.5458888  ",
        "Latitude": "107.6359118 "
    },
    {
        "STT": "70 ",
        "Name": "Trạm y tế thị trấn Phú Đa  ",
        "dia_chi": "Thị trấn Phú Đa, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4089569  ",
        "Latitude": "107.6772521 "
    },
    {
        "STT": "71 ",
        "Name": "Trạm y tế xã Phú An  ",
        "dia_chi": "Xã Phú An, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5169896  ",
        "Latitude": "107.6392892 "
    },
    {
        "STT": "72 ",
        "Name": "Trạm y tế xã Phú Dương  ",
        "dia_chi": "Xã Phú Dương, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5169684  ",
        "Latitude": "107.6067354 "
    },
    {
        "STT": "73 ",
        "Name": "Trạm y tế xã Phú Diên   ",
        "dia_chi": "Xã Phú Diên, Huyện Phú Vang, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4984358  ",
        "Latitude": "107.7221709 "
    },
    {
        "STT": "74 ",
        "Name": "Trạm y tế xã Phú Hải ",
        "dia_chi": "Xã Phú Hải, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5263029  ",
        "Latitude": "107.6955277 "
    },
    {
        "STT": "75 ",
        "Name": "Trạm y tế xã Phú Hồ  ",
        "dia_chi": "Xã Phú Hồ, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4695119  ",
        "Latitude": "107.6629672 "
    },
    {
        "STT": "76 ",
        "Name": "Trạm y tế xã Phú Lương  ",
        "dia_chi": "Xã Phú Lương, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4561073  ",
        "Latitude": "107.6866471 "
    },
    {
        "STT": "77 ",
        "Name": "Trạm y tế xã Phú Mậu ",
        "dia_chi": "Xã Phú Mậu, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.5260024  ",
        "Latitude": "107.5776275 "
    },
    {
        "STT": "78 ",
        "Name": "Trạm y tế xã Phú Mỹ  ",
        "dia_chi": "Xã Phú Mỹ, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4889148  ",
        "Latitude": "107.6381943 "
    },
    {
        "STT": "79 ",
        "Name": "Trạm y tế xã Phú Thanh  ",
        "dia_chi": "Xã Phú Thanh, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.5427868  ",
        "Latitude": "107.6037761 "
    },
    {
        "STT": "80 ",
        "Name": "Trạm y tế xã Phú Thượng ",
        "dia_chi": "Xã Phú Thượng, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4947606  ",
        "Latitude": "107.6008169 "
    },
    {
        "STT": "81 ",
        "Name": "Trạm y tế xã Phú Thuận  ",
        "dia_chi": "Xã Phú Thuận, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.53972 ",
        "Latitude": "107.6718469 "
    },
    {
        "STT": "82 ",
        "Name": "Trạm y tế xã Phú Xuân   ",
        "dia_chi": "Xã Phú Xuân, Huyện Phú Vang, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4891407  ",
        "Latitude": "107.6984879 "
    },
    {
        "STT": "83 ",
        "Name": "Trạm y tế xã Vinh An ",
        "dia_chi": "Xã Vinh An, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4220898  ",
        "Latitude": "107.8169229 "
    },
    {
        "STT": "84 ",
        "Name": "Trạm y tế xã Vinh Hà ",
        "dia_chi": "Xã Vinh Hà, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3664044  ",
        "Latitude": "107.8050772 "
    },
    {
        "STT": "85 ",
        "Name": "Trạm y tế xã Vinh Phú   ",
        "dia_chi": "Xã Vinh Phú, Huyện Phú Vang, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.410229   ",
        "Latitude": "107.7902707 "
    },
    {
        "STT": "86 ",
        "Name": "Trạm y tế xã Vinh Thái  ",
        "dia_chi": "Xã Vinh Thái, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3932029  ",
        "Latitude": "107.7576992 "
    },
    {
        "STT": "87 ",
        "Name": "Trạm y tế xã Vinh Thanh ",
        "dia_chi": "Xã Vinh Thanh, Huyện Phú Vang, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4354994  ",
        "Latitude": "107.7932319 "
    },
    {
        "STT": "88 ",
        "Name": "Trạm y tế xã Vinh Xuân  ",
        "dia_chi": "Xã Vinh Xuân, Huyện Phú Vang, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.460258   ",
        "Latitude": "107.7695429 "
    },
    {
        "STT": "89 ",
        "Name": "Trạm y tế phường  Phú Bài  ",
        "dia_chi": "Thị trấn Phú Bài, Thị xã  Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4003739  ",
        "Latitude": "107.6748069 "
    },
    {
        "STT": "90 ",
        "Name": "Trạm y tế xã Dương Hoà  ",
        "dia_chi": "Xã Dương Hòa, Thị xã  Hương Thủy, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2764731  ",
        "Latitude": "107.5978577 "
    },
    {
        "STT": "91 ",
        "Name": "Trạm y tế xã Phú Sơn ",
        "dia_chi": "Xã Phú Sơn, Thị xã  Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3281385  ",
        "Latitude": "107.6570475 "
    },
    {
        "STT": "92 ",
        "Name": "Trạm y tế xã Thủy Bằng  ",
        "dia_chi": "Xã Thủy Bằng,Thị xã  Hương Thủy, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.395441   ",
        "Latitude": "107.5824946 "
    },
    {
        "STT": "93 ",
        "Name": "Trạm y tế phường Thủy Châu ",
        "dia_chi": "Xã Thủy Châu, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4354507  ",
        "Latitude": "107.6629672 "
    },
    {
        "STT": "94 ",
        "Name": "Trạm y tế phường Thủy Dương   ",
        "dia_chi": "Xã Thủy Dương, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4411867  ",
        "Latitude": "107.620996  "
    },
    {
        "STT": "95 ",
        "Name": "Trạm y tế phường Thủy Lương   ",
        "dia_chi": "Xã Thủy Lương,Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4208019  ",
        "Latitude": "107.6848696 "
    },
    {
        "STT": "96 ",
        "Name": "Trạm y tế phường Thủy Phương  ",
        "dia_chi": "Xã Thủy Phương, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4034364  ",
        "Latitude": "107.6392892 "
    },
    {
        "STT": "97 ",
        "Name": "Trạm y tế xã Thủy Phù   ",
        "dia_chi": "Xã Thủy Phù,Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3694476  ",
        "Latitude": "107.7044085 "
    },
    {
        "STT": "98 ",
        "Name": "Trạm y tế xã Thủy tân   ",
        "dia_chi": "Xã Thủy Tân, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4050476  ",
        "Latitude": "107.7192104 "
    },
    {
        "STT": "99 ",
        "Name": "Trạm y tế xã Thủy Thanh ",
        "dia_chi": "Xã Thủy Thanh, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.4651937  ",
        "Latitude": "107.6416471 "
    },
    {
        "STT": "100   ",
        "Name": "Trạm y tế xã Thủy Vân   ",
        "dia_chi": "Xã Thủy Vân, Thị xã Hương Thủy, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4815299  ",
        "Latitude": "107.6229679 "
    },
    {
        "STT": "101   ",
        "Name": "Trạm y tế thị trấn Lăng Cô ",
        "dia_chi": "Thị trấn Lăng Cô, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2252184  ",
        "Latitude": "108.059866  "
    },
    {
        "STT": "102   ",
        "Name": "Trạm y tế xã Lộc Điền   ",
        "dia_chi": "Xã Lộc Điền, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2458194  ",
        "Latitude": "107.8228459 "
    },
    {
        "STT": "103   ",
        "Name": "Trạm y tế xã Lộc An  ",
        "dia_chi": "Xã Lộc An, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3354587  ",
        "Latitude": "107.7695429 "
    },
    {
        "STT": "104   ",
        "Name": "Trạm y tế xã Lộc Bổn ",
        "dia_chi": "Xã Lộc Bổn, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3478768  ",
        "Latitude": "107.7246879 "
    },
    {
        "STT": "105   ",
        "Name": "Trạm y tế xã Lộc Hoà ",
        "dia_chi": "Xã Lộc Hòa, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2725793  ",
        "Latitude": "107.775465  "
    },
    {
        "STT": "106   ",
        "Name": "Trạm y tế xã Lộc sơn ",
        "dia_chi": "Xã Lộc Sơn, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3261637  ",
        "Latitude": "107.7458559 "
    },
    {
        "STT": "107   ",
        "Name": "Trạm y tế xã Lộc Thủy   ",
        "dia_chi": "Xã Lộc Thủy, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2355544  ",
        "Latitude": "107.9413323 "
    },
    {
        "STT": "108   ",
        "Name": "Trạm y tế xã Vinh Hải   ",
        "dia_chi": "Xã Vinh Hải, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3741349  ",
        "Latitude": "107.879121  "
    },
    {
        "STT": "109   ",
        "Name": "Trạm y tế xã Vinh Hiền  ",
        "dia_chi": "Xã Vinh Hiền, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3354811  ",
        "Latitude": "107.8998566 "
    },
    {
        "STT": "110   ",
        "Name": "Trạm y tế xã Vinh Hưng  ",
        "dia_chi": "Xã Vinh Hưng, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3746558  ",
        "Latitude": "107.8406158 "
    },
    {
        "STT": "111   ",
        "Name": "Trạm y tế xã Vinh Mỹ ",
        "dia_chi": "Xã Vinh Mỹ, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3963054  ",
        "Latitude": "107.852463  "
    },
    {
        "STT": "112   ",
        "Name": "Trạm y tế xã Xuân Lộc   ",
        "dia_chi": "Xã Xuân Lộc, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2560133  ",
        "Latitude": "107.7044085 "
    },
    {
        "STT": "113   ",
        "Name": "Trạm y tế xã Lộc Vĩnh   ",
        "dia_chi": "Xã Lộc Vĩnh, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3045352  ",
        "Latitude": "107.9946667 "
    },
    {
        "STT": "114   ",
        "Name": "Trạm y tế xã Lộc Bình   ",
        "dia_chi": "Xã Lộc Bình, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3220799  ",
        "Latitude": "107.9235563 "
    },
    {
        "STT": "115   ",
        "Name": "Trạm y tế xã Vinh Giang ",
        "dia_chi": "Xã Vinh Giang, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3488826  ",
        "Latitude": "107.8761588 "
    },
    {
        "STT": "116   ",
        "Name": "Trạm y tế xã Lộc Tiến   ",
        "dia_chi": "Xã Lộc Tiến, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2540793  ",
        "Latitude": "107.9887402 "
    },
    {
        "STT": "117   ",
        "Name": "Trạm y tế xã Lộc Trì ",
        "dia_chi": "Xã Lộc Trì, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.219061   ",
        "Latitude": "107.8702347 "
    },
    {
        "STT": "118   ",
        "Name": "Trạm y tế thị trấn Phú Lộc ",
        "dia_chi": "Thị trấn Phú Lộc, Huyện Phú Lộc, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.281899   ",
        "Latitude": "107.8643107 "
    },
    {
        "STT": "119   ",
        "Name": "Trạm y tế thị trấn Khe Tre ",
        "dia_chi": "Thị trấn Khe Tre, Huyện Nam Đông, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.1725519  ",
        "Latitude": "107.7192104 "
    },
    {
        "STT": "120   ",
        "Name": "Trạm y tế xã Hương Hữu  ",
        "dia_chi": "Xã Hương Hữu, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.129175   ",
        "Latitude": "107.6629672 "
    },
    {
        "STT": "121   ",
        "Name": "Trạm y tế xã Hương Hoà  ",
        "dia_chi": "Xã Hương Hoà, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1601535  ",
        "Latitude": "107.6984879 "
    },
    {
        "STT": "122   ",
        "Name": "Trạm y tế xã Hương Lộc  ",
        "dia_chi": "Xã Hương Lộc, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1572066  ",
        "Latitude": "107.7991545 "
    },
    {
        "STT": "123   ",
        "Name": "Trạm y tế xã Hương Phú  ",
        "dia_chi": "Xã Hương Phú, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2086308  ",
        "Latitude": "107.728092  "
    },
    {
        "STT": "124   ",
        "Name": "Trạm y tế xã Hương Sơn  ",
        "dia_chi": "Xã Hương Sơn, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1713675  ",
        "Latitude": "107.63337   "
    },
    {
        "STT": "125   ",
        "Name": "Trạm y tế xã Thượng Lộ  ",
        "dia_chi": "Xã Thượng Lộ, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1139211  ",
        "Latitude": "107.775465  "
    },
    {
        "STT": "126   ",
        "Name": "Trạm y tế xã Thượng Long   ",
        "dia_chi": "Xã Thượng Long, Huyện Nam Đông, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.0755185  ",
        "Latitude": "107.627451  "
    },
    {
        "STT": "127   ",
        "Name": "Trạm y tế xã Thượng Nhật   ",
        "dia_chi": "Xã Thượng Nhật, Huyện Nam Đông, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.0416828  ",
        "Latitude": "107.6925674 "
    },
    {
        "STT": "128   ",
        "Name": "Trạm y tế xã Thượng Quảng  ",
        "dia_chi": "Xã Thượng Quảng, Huyện Nam Đông, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.0990148  ",
        "Latitude": "107.5505152 "
    },
    {
        "STT": "129   ",
        "Name": "Trạm y tế xã Hương Giang   ",
        "dia_chi": "Xã Hương Giang, Huyện Nam Đông, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.150837   ",
        "Latitude": "107.6748069 "
    },
    {
        "STT": "130   ",
        "Name": "Trạm y tế xã A Roằng ",
        "dia_chi": "Xã A Roằng, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.1005733  ",
        "Latitude": "107.3967105 "
    },
    {
        "STT": "131   ",
        "Name": "Trạm y tế xã Hương Nguyên  ",
        "dia_chi": "Xã Hương Nguyên, Huyện A Lưới, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.1977338  ",
        "Latitude": "107.4558555 "
    },
    {
        "STT": "132   ",
        "Name": "Trạm y tế xã Hồng Bắc   ",
        "dia_chi": "Xã Hồng Bắc, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2772874  ",
        "Latitude": "107.1839024 "
    },
    {
        "STT": "133   ",
        "Name": "Trạm y tế xã Hồng Hạ ",
        "dia_chi": "Xã Hồng Hạ, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3005092  ",
        "Latitude": "107.3139304 "
    },
    {
        "STT": "134   ",
        "Name": "Trạm y tế xã Hồng Thượng   ",
        "dia_chi": "Xã Hồng Thượng, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1856859  ",
        "Latitude": "107.2607289 "
    },
    {
        "STT": "135   ",
        "Name": "Trạm y tế xã Hồng Thủy  ",
        "dia_chi": "Xã Hồng Thủy, Huyện A Lưới, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.3553289  ",
        "Latitude": "107.0657549 "
    },
    {
        "STT": "136   ",
        "Name": "Trạm y tế xã Hồng Trung ",
        "dia_chi": "Xã Hồng Trung, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.3247267  ",
        "Latitude": "107.1602683 "
    },
    {
        "STT": "137   ",
        "Name": "Trạm y tế xã Hồng Vân   ",
        "dia_chi": "Xã Hồng Vân, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3514041  ",
        "Latitude": "107.113007  "
    },
    {
        "STT": "138   ",
        "Name": "Trạm y tế xã Phú Vinh   ",
        "dia_chi": "Xã Phú Vinh, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2281581  ",
        "Latitude": "107.2961953 "
    },
    {
        "STT": "139   ",
        "Name": "Trạm y tế xã Sơn Thủy   ",
        "dia_chi": "Xã Sơn Thủy, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2528517  ",
        "Latitude": "107.2725505 "
    },
    {
        "STT": "140   ",
        "Name": "Trạm y tế xã Nhâm ",
        "dia_chi": "Xã Nhâm, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.2318292  ",
        "Latitude": "107.1839024 "
    },
    {
        "STT": "141   ",
        "Name": "Trạm y tế xã Hồng Thái  ",
        "dia_chi": "Xã Hồng Thái, Huyện A Lưới, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.1844238  ",
        "Latitude": "107.2075388 "
    },
    {
        "STT": "142   ",
        "Name": "Trạm y tế xã Hồng Quảng ",
        "dia_chi": "Xã Hồng Quảng, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.2537099  ",
        "Latitude": "107.2282225 "
    },
    {
        "STT": "143   ",
        "Name": "Trạm y tế xã Hồng Kim   ",
        "dia_chi": "Xã Hồng Kim, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.3353635  ",
        "Latitude": "107.2370874 "
    },
    {
        "STT": "144   ",
        "Name": "Trạm y tế xã Hương Phong   ",
        "dia_chi": "Xã Hương Phong, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1745905  ",
        "Latitude": "107.3257545 "
    },
    {
        "STT": "145   ",
        "Name": "Trạm y tế xã Hương Lâm  ",
        "dia_chi": "Xã Hương Lâm, Huyện A Lưới, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.1541066  ",
        "Latitude": "107.3671431 "
    },
    {
        "STT": "146   ",
        "Name": "Trạm y tế xã Bắc Sơn ",
        "dia_chi": "Xã Bắc Sơn, Huyện A Lưới, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.305212   ",
        "Latitude": "107.1898113 "
    },
    {
        "STT": "147   ",
        "Name": "Trạm y tế xã A Ngo   ",
        "dia_chi": "Xã A Ngo, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.2687814  ",
        "Latitude": "107.2518631 "
    },
    {
        "STT": "148   ",
        "Name": "Trạm y tế xã A Đớt   ",
        "dia_chi": "Xã A Đớt, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.075659   ",
        "Latitude": "107.3553171 "
    },
    {
        "STT": "149   ",
        "Name": "Trạm y tế xã Đông Sơn   ",
        "dia_chi": "Xã Đông Sơn, Huyện A Lưới, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.1126512  ",
        "Latitude": "107.3198424 "
    },
    {
        "STT": "150   ",
        "Name": "Trạm y tế thị trấn A Lưới  ",
        "dia_chi": "Thị trấn A Lưới, Huyện A Lưới, Tỉnh Thừa Thiên Huế ",
        "Longtitude": "16.2899011  ",
        "Latitude": "107.2370874 "
    },
    {
        "STT": "151   ",
        "Name": "Trạm y tế phường Hương Sơ  ",
        "dia_chi": "Phường Hương Sơ, Thành phố Huế, Tỉnh Thừa Thiên Huế   ",
        "Longtitude": "16.4887712  ",
        "Latitude": "107.5551545 "
    },
    {
        "STT": "152   ",
        "Name": "Trạm y tế phường An Tây ",
        "dia_chi": "Phường An Tây, Thành phố Huế, Tỉnh Thừa Thiên Huế  ",
        "Longtitude": "16.4266175  ",
        "Latitude": "107.6008169 "
    }
];