﻿var lat = 16.46820;
var lon = 107.56750;
var map = new L.Map('map', {
    minZoom: 8
});

// create a new tile layer
var tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 20
    });

map.addLayer(layer);

map.setView([lat, lon], 16);

var hospitalIcon = L.icon({
    iconUrl: 'images/pin-hopital.png',
    iconSize: [30, 30], // size of the icon
});

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var carIcon = L.icon({
    iconUrl: 'images/ambulance.png',
    iconSize: [36, 36], // size of the icon
});

var lstHospital = data;
// console.log(lstHospital);
var color = null;
var lstColor = [{
        "color": "#003366",
    },
    {
        "color": "#0000FF"
    },
    {
        "color": "#33CC33"
    },
    {
        "color": "#990033"
    },
    {
        "color": "#FF66FF"
    },
    {
        "color": "#CD853F"
    },
    {
        "color": "#CDCD00"
    },
    {
        "color": "#8B658B"
    },
    {
        "color": "#5D478B"
    }
];

var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [16.46054,107.56919],
    [16.46482,107.56599],
    [16.46781,107.57011],
    [16.46919,107.56901],
    // [10.00548,105.08950],
    // [10.00695,105.08834],
    // [10.00706,105.08864],
    [lat, lon]
];
var xecuuthuong2 = [
    [lstHospital[1].lat, lstHospital[1].lon],
    [16.47026,107.57053],
    // [10.00840,105.08410],
    // [10.00993,105.08603],
    // [10.00941,105.08661],
    // [10.00741,105.08798],
    [lat, lon]
];


var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Y Thao Garden Restaurant</span></p>').addTo(map);
marker.openPopup();
var route = null;
var maker = null;


var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
    [20000, 30000, 20000, 10000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT1.loops = 0;
markerCT1.bindPopup();
markerCT1.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 75A - 90378 thuộc: Bệnh viện Đa khoa thành phố Huế<br>Lái xe: Bùi Đức Toàn<br>Bác sỹ: Nông Văn Dũng<br>Điều dưỡng: Phạm Thái Dương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Đạt <br>Còn <strong>15</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT1.start();

var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
    [40000, 30000, 10000, 10000, 10000, 10000], { autostart: true, loop: false, icon: carIcon }).addTo(map);

markerCT2.loops = 0;
markerCT2.bindPopup();
markerCT2.on('mouseover', function(e) {
    //open popup;
    var popup = L.popup()
        .setLatLng(e.latlng)
        .setContent('Xe cứu thương biển số 75A - 76904 thuộc: Bệnh viện Y học Dân tộc Huế<br>Lái xe: Ngô Xuân Hải<br>Bác sỹ: Lưu T. Quỳnh Nga<br>Điều dưỡng: Phạm Thị Thương<br>Địa chỉ: Chợ Hà Lâm<br>Bệnh nhân: Nguyên Văn Long <br>Còn <strong>10</strong> phút nữa hiện trường')
        .openOn(map);
});
markerCT2.start();

var route1 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[0].lat, lstHospital[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "red", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route2 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[1].lat, lstHospital[1].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "blue", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function openModal(idModal) {
    document.getElementById(idModal).style.display = "block";
}