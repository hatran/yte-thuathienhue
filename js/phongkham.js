var dataphongkham = [
 {
    "STT": "1",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "1A Bến Nghé, Phú Hội, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4677335  ",
    "Latitude": "107.5947879 "
},
{
    "STT": "2",
    "Name": "Phòng khám Sản phụ khoa, Chẩn đoán hình ảnh",
    "dia_chi": "78 Ngô Quyền, Vĩnh Ninh, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.461848",
    "Latitude": "107.588896  "
},
{
    "STT": "3",
    "Name": "Phòng khám Răng - Hàm - Mặt",
    "dia_chi": "57 Lý Thường Kiệt, Vĩnh Ninh, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.457015",
    "Latitude": "107.5922261 "
},
{
    "STT": "4",
    "Name": "Phòng khám Sản phụ khoa, Chẩn đoán hình ảnh",
    "dia_chi": "146D Phan Chu Trinh, Phước Vĩnh, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4548488  ",
    "Latitude": "107.5864881 "
},
{
    "STT": "5",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "96B Phan Đăng Lưu, Phú Hòa, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4756047  ",
    "Latitude": "107.5869367 "
},
{
    "STT": "6",
    "Name": "Phòng khám Da liễu ",
    "dia_chi": "G23 Trần Hoành, Trường An, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4420374  ",
    "Latitude": "107.5842454 "
},
{
    "STT": "7",
    "Name": "Phòng khám Sản phụ khoa ",
    "dia_chi": "9/63 Điện Biên Phủ, Vĩnh Ninh, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4541753  ",
    "Latitude": "107.5812256 "
},
{
    "STT": "8",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "22 Nguyễn Tri Phương, Phú Nhuận, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4644893  ",
    "Latitude": "107.5920991 "
},
{
    "STT": "9",
    "Name": "Phòng khám Da liễu ",
    "dia_chi": "62 Hai Bà Trưng, thành phố Huế, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.459665",
    "Latitude": "107.588941  "
},
{
    "STT": "10 ",
    "Name": "Phòng khám Sản phụ khoa ",
    "dia_chi": "6/11 Lý Thường Kiệt, Phú Nhuận, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4617057  ",
    "Latitude": "107.5913918 "
},
{
    "STT": "11 ",
    "Name": "Phòng khám Nhi ",
    "dia_chi": "18 Nguyễn Công Trứ, Phú Hội, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4715291  ",
    "Latitude": "107.5968628 "
},
{
    "STT": "12 ",
    "Name": "Phòng khám Thận - Tiết niệu, Nam khoa",
    "dia_chi": "55 Nguyễn Chí Diễu, Thuận Thành, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.474056",
    "Latitude": "107.582151  "
},
{
    "STT": "13 ",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "61B đường 68, Thuận Thành, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4717352  ",
    "Latitude": "107.5796877 "
},
{
    "STT": "14 ",
    "Name": "Phòng khám Nhi ",
    "dia_chi": "1 Trần Quốc Toản, Tây Lộc, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4722009  ",
    "Latitude": "107.566774  "
},
{
    "STT": "15 ",
    "Name": "Phòng khám Dinh dưỡng, Nhi ",
    "dia_chi": "57A Đặng Huy Trứ, Trường An, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.4476192  ",
    "Latitude": "107.586469  "
},
{
    "STT": "16 ",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "583 Lạc Long Quân, Lăng Cô, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.2341284  ",
    "Latitude": "108.0824188 "
},
{
    "STT": "17 ",
    "Name": "Phòng khám Khám bệnh, Giải phẫu bệnh ",
    "dia_chi": "81A Trần Quốc Toản, Tây Lộc, thành phố Huế, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.475276",
    "Latitude": "107.564437  "
},
{
    "STT": "18 ",
    "Name": "Phòng khám Răng - Hàm - Mặt",
    "dia_chi": "66 Hùng Vương, Phú Nhuận, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4612256  ",
    "Latitude": "107.5971321 "
},
{
    "STT": "19 ",
    "Name": "Phòng khám Khám bệnh, Giải phẫu bệnh ",
    "dia_chi": "40 Xuân Diệu, Trường An, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4481923  ",
    "Latitude": "107.5854566 "
},
{
    "STT": "20 ",
    "Name": "Phòng khám Khám bệnh ",
    "dia_chi": "224 Nguyễn Trãi, Tây Lộc, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.47627 ",
    "Latitude": "107.566246  "
},
{
    "STT": "21 ",
    "Name": "Phòng khám Sản phụ khoa ",
    "dia_chi": "Phú Khê, Phú Dương, Phú Vang, Tỉnh Thừa Thiên Huế",
    "Longtitude": "16.511197",
    "Latitude": "107.6006927 "
},
{
    "STT": "22 ",
    "Name": "Phòng khám Đa khoa ",
    "dia_chi": "123 Trần Hưng Đạo, thành phố Huế, Tỉnh Thừa Thiên Huế ",
    "Longtitude": "16.4702309  ",
    "Latitude": "107.5865096 "
}
];