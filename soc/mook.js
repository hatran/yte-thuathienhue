var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Đa khoa thành phố Huế
        Bệnh viện Đại học Y dược Huế
        Bệnh viện Hương Thuỷ
        Bệnh viện Đa khoa huyện Quảng Điền
        Trung tâm Y tế huyện Nam Đồng
        Trung tâm Y tế huyện Nam Đồng
        Bệnh viện điều dưỡng phục hồi chức năng Huế`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Đa khoa thành phố Huế
        Bệnh viện Đại học Y dược Huế
        Bệnh viện Hương Thuỷ
        Bệnh viện Đa khoa huyện Quảng Điền
        Trung tâm Y tế huyện Nam Đồng
        Trung tâm Y tế huyện Nam Đồng
        Bệnh viện điều dưỡng phục hồi chức năng Huế`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP Huế‎‎
                H. Nam Đông
                Bình Liêu‎
                H. A Lưới‎‎
                H. Phong Điền
                H. Phú Lộc‎‎
                Đông Triều‎ 
                H. Phú Vang‎
                TX Quảng Điền‎‎ 
                TX Hương Thủy‎
                Quảng Yên‎
                Tiên Yên‎
                TX Hương Trà‎ 
                Vân Đồn‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Đa khoa thành phố Huế
        Bệnh viện Đại học Y dược Huế
        Bệnh viện Hương Thuỷ
        Bệnh viện Đa khoa huyện Quảng Điền
        Trung tâm Y tế huyện Nam Đồng
        Trung tâm Y tế huyện Nam Đồng
        Bệnh viện điều dưỡng phục hồi chức năng Huế`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Đa khoa thành phố Huế
        Bệnh viện Đại học Y dược Huế
        Bệnh viện Hương Thuỷ
        Bệnh viện Đa khoa huyện Quảng Điền
        Trung tâm Y tế huyện Nam Đồng
        Trung tâm Y tế huyện Nam Đồng
        Bệnh viện điều dưỡng phục hồi chức năng Huế`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
