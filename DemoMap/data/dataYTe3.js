var data = [
{
    "STT": 1,
    "Name": "Bệnh viện Đa Khoa tỉnh THỪA THIÊN HUẾ",
    "address": "Tuệ Tĩnh, Lán Bè, Bạch Đằng, Hạ Long, Thành phố Hạ Long, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.95416,
    "lon": 107.08856,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 9,
    "Name": "Bệnh viện Đa khoa H. A Lưới‎",
    "address": "Ngõ 371 Trần Phú, Cẩm Thành, H. A Lưới‎, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 21.00911,
    "lon": 107.27704,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 2,
    "Name": "Bệnh viện Bãi Cháy",
    "address": "Hạ Long, Cái Dăm, Giếng Đáy, Hạ Long, Thành phố Hạ Long, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.97622,
    "lon": 107.01368,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 3,
    "Name": "Bệnh viện Đa khoa Vinmec Hạ Long",
    "address": "Lê Thánh Tông, Lán Bè, Hồng Gai, Hạ Long, Thành phố Hạ Long, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.95191,
    "lon": 107.07186,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 4,
    "Name": "Bệnh viện Quốc tế Hạ Long",
    "address": "Hùng Thắng, Cái Dăm, Hùng Thắng, Hạ Long, Thành phố Hạ Long, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.96088,
    "lon": 107.01494,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 5,
    "Name": "Bệnh viện Phục hồi Chức năng THỪA THIÊN HUẾ",
    "address": "Đặng Châu Tuệ, Quang Hanh, H. A Lưới‎, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.98495,
    "lon": 107.19873,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 6,
    "Name": "Bệnh viện Y Dược Cổ truyền THỪA THIÊN HUẾ",
    "address": "Nguyễn Văn Cừ, Khu phố 3, Hồng Hà, Hạ Long, Thành phố Hạ Long, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 20.95131,
    "lon": 107.13137,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 7,
    "Name": "Bệnh viện Đa khoa Móng Cái",
    "address": "Đại lộ Hòa Bình, Đông Thịnh, Hoà Lạc, Móng Cái, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 21.52491,
    "lon": 107.96112,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 8,
    "Name": "Bệnh viện Đa khoa khu vực H. A Lưới‎",
    "address": "Trần Quốc Tảng, Cẩm Thịnh, H. A Lưới‎, Tỉnh THỪA THIÊN HUẾ, Việt Nam",
    "lat": 21.00603,
    "lon": 107.34916,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  }
];