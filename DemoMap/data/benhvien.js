var data = [
  {
    "STT": 1,
    "Name": "Bệnh viện Đa khoa thành phố Huế",
    "address": "Phường Đại Yên, Thành Phố Hạ Long, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 20.9656543,
    "Latitude": 106.9367405,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 2,
    "Name": "Khu Điều trị Phong",
    "address": "Phường Minh Thành, Thị xã Quảng Yên, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.183941,
    "Latitude": 106.056133,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 3,
    "Name": "Bệnh viện Hương Thuỷ",
    "address": "Phường Đức Chính, Thị xã Đông Triều, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.076771,
    "Latitude": 106.522836,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 4,
    "Name": "Bệnh viện Đa khoa huyện Quảng Điền",
    "address": "Thôn Cửa Tràng, Phường Tiền An, Xã Yên Hưng, Thị xã Quảng YênTỉnh THỪA THIÊN HUẾ",
    "Longtitude": 20.9384382,
    "Latitude": 106.8315307,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 5,
    "Name": "Trung tâm Y tế huyện Nam Đồng",
    "address": "445 đường Trần Phú, Phường Cẩm Thành, Thành phố H. A Lưới‎, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.0098173,
    "Latitude": 107.2768486,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 6,
    "Name": "Trung tâm Y tế huyện Nam Đồng",
    "address": "Khu 2, Thị trấn Trới, Huyện TX Quảng Điền‎, Tỉnh THỪA THIÊN HUẾ ",
    "Longtitude": 21.0396647,
    "Latitude": 106.9889904,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 7,
    "Name": "Bệnh viện điều dưỡng phục hồi chức năng Huế",
    "address": "Thôn 12 xã Hạ Long, huyện Vân Đồn, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.0746625,
    "Latitude": 107.4266536,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 8,
    "Name": "Bệnh viện Đa khoa huyện A Lưới",
    "address": "Khu 1, thị trấn Cô Tô, huyện Cô Tô, tỉnh Tỉnh THỪA THIÊN HUẾ ",
    "Longtitude": 20.993715,
    "Latitude": 107.7576992,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 9,
    "Name": "Bệnh viện Đa khoa huyện Phú Lộc",
    "address": "Khu 4 thị trấn H. A Lưới, H. A Lưới‎, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.2717717,
    "Latitude": 107.2892109,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 10,
    "Name": "Trung tâm Y tế huyện Hương Trà",
    "address": "Phố Lý Thường Kiệt, Thị Trấn Tiên Yên, Huyện Tiên Yên, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.3325573,
    "Latitude": 107.4019703,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 11,
    "Name": "Bệnh viện Y học Dân tộc Huế",
    "address": "Khu Bình An, Thị trấn Huyện Bình Liêu, Lục Hồn, Bình Liêu, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.5290037,
    "Latitude": 107.4010657,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 12,
    "Name": "Bệnh viện Ngoại khoa Nguyễn Văn Thái",
    "address": "Phố Lỷ A Coỏng, thị trấn H. Phú Lộc‎, huyện H. Phú Lộc‎, Tỉnh THỪA THIÊN HUẾ",
    "Longtitude": 21.354971,
    "Latitude": 107.595497,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 13,
    "Name": "Bệnh viện Đa khoa huyện Phong Điền",
    "address": "Số 10 Trần Quốc Toản, Thị trấn Quảng Hà, Huyện Hải Hà, Tỉnh THỪA THIÊN HUẾ  ",
    "Longtitude": 21.4518704,
    "Latitude": 107.7493572,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  },
  {
    "STT": 14,
    "Name": "Trung tâm Phục hồi chức năng trẻ tàn tật Huế",
    "address": "Đường Tuệ Tĩnh, phường Ninh Dương, Thành phố Móng Cái, Tỉnh THỪA THIÊN HUẾ ",
    "Longtitude": 21.5249465,
    "Latitude": 107.9610501,
    "Rating": 4.3,
    "Number_of_beds": "600",
    "area": "Tỉnh"
  }
];